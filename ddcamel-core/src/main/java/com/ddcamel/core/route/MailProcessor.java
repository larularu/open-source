package com.ddcamel.core.route;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.StringReader;
import java.util.Base64;
import java.util.Properties;

import javax.mail.Message.RecipientType;
import javax.mail.MessagingException;
import javax.mail.Session;
import javax.mail.internet.MimeMessage;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.Unmarshaller;

import org.apache.camel.Exchange;
import org.apache.camel.Processor;

import com.ddcore.bean.EmailContent;
import com.google.api.services.gmail.model.Message;

public class MailProcessor implements Processor {

	private static final String USER_ID = "me";

	@Override
	public void process(Exchange exchange) throws Exception {
		String data = exchange.getIn().getBody(String.class);

		JAXBContext jaxbContext = JAXBContext.newInstance(EmailContent.class);
		Unmarshaller jaxbUnmarshaller = jaxbContext.createUnmarshaller();
		EmailContent email = (EmailContent) jaxbUnmarshaller.unmarshal(new StringReader(data));
		Message content = createMessage(email);

		exchange.getIn().setHeader("CamelGoogleMail.userId", USER_ID);
		exchange.getIn().setHeader("CamelGoogleMail.content", content);
	}

	private Message createMessage(EmailContent email) throws MessagingException, IOException {

		Session session = Session.getDefaultInstance(new Properties(), null);
		MimeMessage mm = new MimeMessage(session);

		try {
			mm.addRecipients(RecipientType.TO, email.getTo());
			mm.setSubject(email.getSubject());
			mm.setContent(email.getContent(), "text/html; charset=UTF-8");
		} catch (Exception e) {
			e.printStackTrace();
		}

		Message message = new Message();
		try (ByteArrayOutputStream baos = new ByteArrayOutputStream()) {
			mm.writeTo(baos);
			message.setRaw(Base64.getUrlEncoder().encodeToString(baos.toByteArray()));
		}
		return message;
	}

}