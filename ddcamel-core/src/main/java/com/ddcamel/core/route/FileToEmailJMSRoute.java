package com.ddcamel.core.route;

import org.apache.camel.builder.RouteBuilder;

public class FileToEmailJMSRoute extends RouteBuilder {

	@Override
	public void configure() throws Exception {
		// .to("log:?level=INFO&showBody=true").to("jms:customer/email");
		
		// from("file:/email?readLock=changed&readLockMinAge=30s&move=archive")
		// .log("Processing file:${header.CamelFileName}").to("jms:customer/email");
		
		from("file:/email?readLock=changed&readLockMinAge=5s&move=archive").to("jms:customer/email");
	}

}
