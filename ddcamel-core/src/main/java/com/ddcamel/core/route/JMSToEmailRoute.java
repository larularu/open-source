package com.ddcamel.core.route;

import java.io.InputStream;
import java.io.StringReader;
import java.util.Properties;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.Unmarshaller;

import org.apache.camel.Exchange;
import org.apache.camel.Processor;
import org.apache.camel.builder.RouteBuilder;

import com.ddcore.bean.EmailContent;
import com.ddcore.security.DDEncryption;
import com.ddcore.security.EnvKey;

public class JMSToEmailRoute extends RouteBuilder {

	@Override
	public void configure() throws Exception {

		Properties prop = new Properties();

		InputStream inputStream = JMSToEmailRoute.class.getClassLoader().getResourceAsStream("smtp.properties");
		prop.load(inputStream);

		String smtp = prop.getProperty("smtp");
		String username = prop.getProperty("username");
		String password = prop.getProperty("password");
		String to_smtp = "smtps://" + smtp + "?username=" + DDEncryption.decrypt(username, EnvKey.getEnvEncodedKey())
				+ "&password=RAW(" + DDEncryption.decrypt(password, EnvKey.getEnvEncodedKey()) + ")&debugMode=false";

		from("jms:customer/email").process(new Processor() {
			@Override
			public void process(final Exchange exchange) throws Exception {
				String data = exchange.getIn().getBody(String.class);

				JAXBContext jaxbContext = JAXBContext.newInstance(EmailContent.class);
				Unmarshaller jaxbUnmarshaller = jaxbContext.createUnmarshaller();
				EmailContent email = (EmailContent) jaxbUnmarshaller.unmarshal(new StringReader(data));

				exchange.getIn().setHeader("contentType", "text/html");
				exchange.getIn().setHeader("from", email.getFrom());
				exchange.getIn().setHeader("to",
						(email.getTo() != null ? DDEncryption.decrypt(email.getTo(), EnvKey.getEnvEncodedKey())
								: null));
				exchange.getIn().setHeader("bcc",
						(email.getBcc() != null ? DDEncryption.decrypt(email.getBcc(), EnvKey.getEnvEncodedKey())
								: null));
				exchange.getIn().setHeader("subject",
						(email.getSubject() != null
								? DDEncryption.decrypt(email.getSubject(), EnvKey.getEnvEncodedKey())
								: null));
				exchange.getIn()
						.setBody((email.getContent() != null
								? DDEncryption.decrypt(email.getContent(), EnvKey.getEnvEncodedKey())
								: null));
			}
		}).to(to_smtp);
		// in case of logging needed
		// .to("log:?level=INFO&showBody=true").to(to_smtp);
	}

}
