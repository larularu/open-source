package com.ddcamel.core.route;

import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;
import java.util.Base64;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Properties;

import javax.mail.Message.RecipientType;
import javax.mail.MessagingException;
import javax.mail.Session;
import javax.mail.internet.MimeMessage;

import org.apache.camel.builder.RouteBuilder;
import org.json.JSONObject;

import com.ddcore.bean.EmailContent;
import com.google.api.services.gmail.model.Message;

public class JMSToGmailRoute extends RouteBuilder {

	// private static final String EMAIL_SUBJECT = "Camel Google Mail Test";
	// private static final String USER_ID = "me";

	@Override
	public void configure() throws Exception {

		Properties prop = new Properties();

		InputStream inputStream = JMSToGmailRoute.class.getClassLoader().getResourceAsStream("smtp.properties");
		prop.load(inputStream);

		String clientId = "<your client id>";
		String secretId = "<your secret id>";
		String accessToken = getAccessToken();

		String to_gmail = "google-mail:messages/send?applicationName=<your application name>=" + clientId
				+ "&clientSecret=" + secretId + "&accessToken=" + accessToken;

		from("jms:customer/email").process(new MailProcessor()).to(to_gmail);
		// .to("log:?level=INFO&showBody=true")
	}

	private static String getAccessToken() {
		try {
			Map<String, Object> params = new LinkedHashMap<>();
			params.put("grant_type", "refresh_token");
			params.put("client_id", "<your client id>");
			params.put("client_secret", "<your secret id>");
			params.put("refresh_token", "<your token refresh>");

			StringBuilder postData = new StringBuilder();
			for (Map.Entry<String, Object> param : params.entrySet()) {
				if (postData.length() != 0) {
					postData.append('&');
				}
				postData.append(URLEncoder.encode(param.getKey(), "UTF-8"));
				postData.append('=');
				postData.append(URLEncoder.encode(String.valueOf(param.getValue()), "UTF-8"));
			}
			byte[] postDataBytes = postData.toString().getBytes("UTF-8");

			URL url = new URL("https://accounts.google.com/o/oauth2/token");
			HttpURLConnection con = (HttpURLConnection) url.openConnection();
			con.setDoOutput(true);
			con.setUseCaches(false);
			con.setRequestMethod("POST");
			con.getOutputStream().write(postDataBytes);

			BufferedReader reader = new BufferedReader(
					new InputStreamReader(con.getInputStream(), StandardCharsets.UTF_8));
			StringBuffer buffer = new StringBuffer();
			for (String line = reader.readLine(); line != null; line = reader.readLine()) {
				buffer.append(line);
			}
			reader.close();

			JSONObject json = new JSONObject(buffer.toString());
			String accessToken = json.getString("access_token");
			System.out.println("accessToken: " + accessToken);
			return accessToken;
		} catch (IOException ex) {
			ex.printStackTrace();
		}
		return null;
	}

	private Message createMessage(EmailContent email) throws MessagingException, IOException {

		Session session = Session.getDefaultInstance(new Properties(), null);
		MimeMessage mm = new MimeMessage(session);

		try {
			mm.addRecipients(RecipientType.TO, email.getTo());
			mm.setSubject(email.getSubject());
			mm.setContent(email.getContent(), "text/html; charset=UTF-8");
		} catch (Exception e) {
			e.printStackTrace();
		}

		Message message = new Message();
		try (ByteArrayOutputStream baos = new ByteArrayOutputStream()) {
			mm.writeTo(baos);
			message.setRaw(Base64.getUrlEncoder().encodeToString(baos.toByteArray()));
		}
		return message;
	}

}
