package com.ddcamel.core.singleton;

import javax.jms.ConnectionFactory;

import org.apache.activemq.ActiveMQConnectionFactory;
import org.apache.camel.CamelContext;
import org.apache.camel.component.jms.JmsComponent;
import org.apache.camel.impl.DefaultCamelContext;

public class JmsConnectionDDFactory {

	public static CamelContext createCamelContext() throws Exception {
		// create CamelContext
		CamelContext camelContext = new DefaultCamelContext();
		// connect to embedded ActiveMQ JMS broker
		ConnectionFactory connectionFactory = new ActiveMQConnectionFactory("tcp://localhost:61616");
		camelContext.addComponent("jms", JmsComponent.jmsComponentAutoAcknowledge(connectionFactory));
		return camelContext;
	}

}
