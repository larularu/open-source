package com.dra.service;

import java.util.logging.Logger;

import com.dra.events.Events;
import com.dra.model.Address;
import com.dra.model.User;
import com.dra.queue.MessageQueue;
import com.google.gson.Gson;
import com.google.gson.JsonObject;

public class UserService {

	private Logger logger = Logger.getLogger(UserService.class.getName());

	public User updateUser(User user, Address address) {
		user.updateUserAddress(address);
		publishEvent(Events.CHANGE_ADDRESS_CONFIRMATION, user.getEmail());
		return user;
	}

	private void publishEvent(Events event, String emailAddress) {

		JsonObject object = new JsonObject();
		object.addProperty("event", event.event);
		object.addProperty("emailAddress", emailAddress);
		String JSON = new Gson().toJson(object);

		MessageQueue.sendEvent(JSON);
		logger.info("UPDATING USER ADDRESS WITH " + JSON);
	}
}