package com.dra.model;

public class Address {

	private String adress;
	private Integer number;
	private String zipCode;

	public Address(String adress, Integer number, String zipCode) {
		this.adress = adress;
		this.number = number;
		this.zipCode = zipCode;
	}

	public String getAdress() {
		return adress;
	}

	public void setAdress(String adress) {
		this.adress = adress;
	}

	public Integer getNumber() {
		return number;
	}

	public void setNumber(Integer number) {
		this.number = number;
	}

	public String getZipCode() {
		return zipCode;
	}

	public void setZipCode(String zipCode) {
		this.zipCode = zipCode;
	}

}